/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "/Users/harrygardiner/Desktop/SDA/07 Basic Audio Threading/Builds/MacOSX/SinOscillator.hpp"


/** Class containing all audio processes */

class Audio :  public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    
    
private:
    AudioDeviceManager audioDeviceManager;
    SinOsc sinOsc;
    
    
};

#endif  // AUDIO_H_INCLUDED
