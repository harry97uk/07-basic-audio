/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"


Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    
}


void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float sinVal;
    float velocity;
    
    while(numSamples--)
    {
        
        velocity = sinOsc.gain();
        sinVal = sinOsc.phase();
        
        *outL = sinVal * velocity;
        *outR = sinVal * velocity;
        
        inL++;
        inR++;
        outL++;
        outR++;
        
        
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
}

void Audio::audioDeviceStopped()
{

}

