//
//  SinOscillator.hpp
//  JuceBasicAudio - App
//
//  Created by Harry Gardiner on 13/11/2017.
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"

class SinOsc  :  public MidiInputCallback
{
public:
    SinOsc();
    
    ~SinOsc();
    
    void handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message) override;
    
    float phase();
    
    float gain();
    
private:
    
    MidiMessage midiMessage;
    float frequency;
    float phasePosition;
    float sampleRate;
    float sinVal;
    float velocity;
    AudioDeviceManager audioDeviceManager;
};

#endif /* SinOscillator_hpp */
