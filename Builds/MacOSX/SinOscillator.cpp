//
//  SinOscillator.cpp
//  JuceBasicAudio - App
//
//  Created by Harry Gardiner on 13/11/2017.
//

#include "SinOscillator.hpp"

SinOsc::SinOsc()
{
    audioDeviceManager.addMidiInputCallback(String::empty, this);
}
SinOsc::~SinOsc()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void SinOsc::handleIncomingMidiMessage(MidiInput *source,const MidiMessage &message)
{
    
    frequency = midiMessage.getMidiNoteInHertz(midiMessage.getNoteNumber());
    velocity = midiMessage.getVelocity();
}

float SinOsc::phase()
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency)/sampleRate;
    
    phasePosition = phasePosition + phaseIncrement;
    if (phasePosition > twoPi)
    {
        phasePosition = phasePosition - twoPi;
    }
    sinVal = sin(phasePosition);
    
    return sinVal;
}

float SinOsc::gain()
{
    return velocity;
}
